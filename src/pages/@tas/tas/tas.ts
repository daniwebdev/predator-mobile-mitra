import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { KonsumenPage } from '../../@konsumen/konsumen/konsumen';
import { HistoryMapPage } from '../history-map/history-map';
import { PerfomaPage } from '../perfoma/perfoma';
import { StatusMapPage } from '../status-map/status-map';
import { KomisiPage } from '../komisi/komisi';
import { HotProspekPage } from '../hot-prospek/hot-prospek';

@IonicPage()
@Component({
  selector: 'page-tas',
  templateUrl: 'tas.html',
})
export class TasPage {

  imageSlider;
  menu;

  constructor(
    public navCtrl: NavController,
    public api: ApiServiceProvider,
    public navParams: NavParams) {
  }
  ionViewDidLoad() {
    if(!this.imageSlider) {
      this.api.Get('promosi/images/slider').then((slider: any) => {
        var imageSlider = slider.data;
        for(let index in imageSlider) {
          imageSlider[index] = {
            image: imageSlider[index].image,
            name: imageSlider[index].nama_kategori
          }
        }
        this.imageSlider = imageSlider
      }).catch((err) => {
        alert('Error :'+JSON.stringify(err.Error))
      }) //End Get Slider
    }

    if(!this.menu) {
      this.api.Get('mobile/icons/tas_sales').then((res: any) => {
        let output = [];
        for(let item in res) {
          let nama   = (item.split('_').join(' '))
          let name   = nama.charAt(0).toUpperCase() + nama.slice(1);
          output.push({
            name: name,
            kode: item,
            image: res[item]
          })
        }
        this.menu = output
      })
    }
  }

  goChild(page='') {
    console.log(page)
    if(page === 'konsumen') {
      this.navCtrl.push(KonsumenPage)
    }
    
    if(page === 'history') {
      this.navCtrl.push(HistoryMapPage)
    }

    if(page === "performa") {
      this.navCtrl.push(PerfomaPage)
    }
    if(page === 'status_map') {
      this.navCtrl.push(StatusMapPage)
    }
    if(page === 'komisi') {
      this.navCtrl.push(KomisiPage)
    }
    if(page === 'hot_prospek'){
      this.navCtrl.push(HotProspekPage)
    }
  }

}
