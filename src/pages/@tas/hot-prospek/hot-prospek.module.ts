import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HotProspekPage } from './hot-prospek';

@NgModule({
  declarations: [
    HotProspekPage,
  ],
  imports: [
    IonicPageModule.forChild(HotProspekPage),
  ],
})
export class HotProspekPageModule {}
