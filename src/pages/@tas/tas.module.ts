import { NgModule } from '@angular/core';
import { HistoryMapPageModule } from "./history-map/history-map.module";
import { TasPageModule } from "./tas/tas.module";
import { PerfomaPageModule } from './perfoma/perfoma.module';
import { StatusMapPageModule } from './status-map/status-map.module';
import { KomisiPageModule } from './komisi/komisi.module';
import { HotProspekPageModule } from './hot-prospek/hot-prospek.module';

@NgModule({
  declarations: [
  ],
  imports: [
    PerfomaPageModule,
    StatusMapPageModule,
    KomisiPageModule,
    HotProspekPageModule,
    TasPageModule,
    HistoryMapPageModule
  ],
})
export class TasModule {}
