import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

/**
 * Generated class for the BeritaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-berita',
  templateUrl: 'berita.html',
})
export class BeritaPage {
  
  berita      = 'cabang';
  beritaPusat;
  beritaCabang;

  constructor(
    public navCtrl: NavController, 
    public api: ApiServiceProvider,
    public navParams: NavParams
    ) {}

  ionViewDidLoad() {
    this.api.Get('promosi/Berita/pusat').then((res: any) => {
      this.beritaPusat = res.data.status ? res.data:[];
    }).catch(errr => {
      let m = errr.error.message
      alert('Error memuat berita. => '+m)
    })

    this.api.Get('promosi/Berita/cabang').then((res: any) => {
      
      this.beritaCabang = res.data.status ? res.data:[];

    }).catch(errr => {
      
      let m = errr.error.message
      alert('Error memuat berita. => '+m)

    })
  }

}
