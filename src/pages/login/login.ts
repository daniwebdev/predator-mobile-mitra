import { Component } from '@angular/core';
import { Storage } from "@ionic/storage";
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import { TabsPage } from "../../pages/tabs/tabs";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  username;
  password;
  respond;
  slider;

  constructor(
    public api: ApiServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public loadingCtrl: LoadingController
    ) {
  }

  ionViewWillEnter() {}

  login() {
    
    var loading = this.loadingCtrl.create({
      content: 'Mohon Tunggu...'
    })    
    let body = new FormData();
    body.append('username', this.username);
    body.append('password', this.password);

    if(this.username&&this.password) {
      loading.present();
      this.api.Post('auth/request_token', body).then((res: any) => {
        loading.dismiss();
        this.storage.set('token', res.data.token)
        this.navCtrl.setRoot(TabsPage);
      }).catch(err => {
        alert(err.error.message)
        loading.dismiss();
      })
    } else {
      alert('Username & Password tidak boleh Kosong.')
    }
  }

}
