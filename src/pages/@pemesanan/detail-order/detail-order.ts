import { Component } from '@angular/core';
import { Storage } from "@ionic/storage";
import { IonicPage, NavController, NavParams, LoadingController, ModalController, ViewController } from 'ionic-angular';
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { TabsPage } from '../../tabs/tabs';
/**
 * Generated class for the DetailOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-order',
  templateUrl: 'detail-order.html',
})

export class DetailOrderPage {

  dataPemohon;
  dataPendamping;
  id_konsumen;
  dataPesanan;
  dataRincian;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private storage: Storage,
    private loading: LoadingController,
    private modal: ModalController ,
    public api: ApiServiceProvider) {
    
      this.dataPemohon    = navParams.get('namaPemohon')
      this.dataPendamping = navParams.get('namaPendamping')
      this.id_konsumen    = navParams.get('id_konsumen')

    if(!navParams.get('namaPemohon')) {
      this.storage.get('dataPrev').then(data => {
        data = JSON.parse(data);
        this.dataPemohon    = data.data.pemohon;
        this.id_konsumen    = data.data.id
        this.dataPendamping = data.data.pendamping;
        this.storage.remove('dataPrev')
      })
    }
    this.GetCart()
  }

  ionViewDidLoad() {
    
  }

  GetCart() {
    this.api.Get('pemesanan/cart/get').then((res: any) => {
      this.dataPesanan             = res.data.result ? res.data.result:false
      console.log(this.dataPesanan)
      this.dataRincian             = res.data
    }).catch(err => {

    })
  }

  hapusCart(idCart) {
    let loading = this.loading.create({
      content: 'Mohon Tunggu...'
    });
    loading.present();
    this.api.Get('pemesanan/cart/delete?cart_id='+idCart).then((res: any) => {
      if(res.status) {
        this.GetCart()
      }
      loading.dismiss();
    }).catch((error) => {
      alert('Error!')
      loading.dismiss();
    })
  }

  tambahPesanan() {
    let Data = {
      toPage: 'DetailOrderPage',
      fromPage: 'DetailProdukPage',
      data: {
        id: this.id_konsumen,
        pemohon:  this.dataPemohon,
        pendamping: this.dataPendamping
      }
    }

    this.storage.set('dataPrev', JSON.stringify(Data))
    this.navCtrl.popToRoot()
  }

  ajukan() {
    let modalAjukan = this.modal.create(ModalAjukan, {
      id_konsumen: this.id_konsumen 
    })

    modalAjukan.present();
  }

}


@Component({
  selector: 'page-ajukan-order',
  templateUrl: 'child/ajukan-order.html',
})
export class ModalAjukan {
  
  tanggal_survey;
  keterangan;

  constructor(
    private params: NavParams,
    private view: ViewController,
    private navCtrl: NavController,
    private api: ApiServiceProvider,
    private loading: LoadingController
  ) {}

  ajukan() {
    let form = new FormData();
    let id_konsumen = this.params.get('id_konsumen');
    form.append('konsumen_id', id_konsumen)
    form.append('tanggal_survey', this.tanggal_survey)
    form.append('keterangan', this.keterangan)
    let loading = this.loading.create({
      content: "Mohon Tunggu...</br><small>Proses Pengajuan."
    })
    if(this.tanggal_survey && this.keterangan && id_konsumen) {
      loading.present();
      this.api.post('pemesanan/sewabeli/save', form, res => {
        loading.dismiss();
        if(res.status == true) {
          alert(res.message)
          this.navCtrl.setRoot(TabsPage)
        } else {
          alert(res.error.message)
        }
      })
    } else {
      // this.navCtrl.popToRoot()
      alert('Form Tanggal Survey & Keterangan Harus di isi.')
    }
  }

  close() {
    this.view.dismiss()
  }
}