import { NgModule } from '@angular/core';
import { IonicPageModule, Content } from 'ionic-angular';
import { KonsumenAddPage } from './konsumen-add';
import { Geolocation } from "@ionic-native/geolocation";
import { SelectSearchableModule } from "ionic-select-searchable";
import { Camera } from "@ionic-native/camera";

@NgModule({
  declarations: [
    KonsumenAddPage,
  ],
  imports: [
    SelectSearchableModule,
    IonicPageModule.forChild(KonsumenAddPage),
  ],
  providers: [
    Camera,
    Content,
    Geolocation
  ]
})
export class KonsumenAddPageModule {}