import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KonsumenPage } from './konsumen';

@NgModule({
  declarations: [
    KonsumenPage,
  ],
  imports: [
    IonicPageModule.forChild(KonsumenPage),
  ],
})
export class KonsumenPageModule {}
