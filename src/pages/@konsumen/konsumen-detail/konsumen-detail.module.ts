import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KonsumenDetailPage, ModalDetailKonsumen } from './konsumen-detail';
import { LaunchNavigator } from "@ionic-native/launch-navigator";

@NgModule({
  declarations: [
    KonsumenDetailPage,
    ModalDetailKonsumen,
  ],
  entryComponents: [
    ModalDetailKonsumen
  ],
  imports: [
    IonicPageModule.forChild(KonsumenDetailPage),
  ],
  providers: [
    LaunchNavigator
  ]
})
export class KonsumenDetailPageModule {}
