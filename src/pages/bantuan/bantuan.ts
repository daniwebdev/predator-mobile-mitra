import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, } from 'ionic-angular';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { ApiServiceProvider } from "../../providers/api-service/api-service"
import { shouldCallLifecycleInitHook } from '@angular/core/src/view';

@IonicPage()
@Component({
  selector: 'page-bantuan',
  templateUrl: 'bantuan.html',
})
export class BantuanPage {

  tutorialVideo = []
  listLinkYT: SafeResourceUrl;
  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams, 
    private api: ApiServiceProvider,
    private domSanitizer: DomSanitizer
    ) {
    
  }

  ionViewDidLoad(): void {
    let key     = "AIzaSyC459mh-NcKXk2_R_SS79L-mP2KT_X9xnQ";
    let channel = 'UCvtbw3Iq_AM7a18iRFqGC7w'
    let maxRes  = 5
    let url     = 'https://content.googleapis.com/youtube/v3/search?part=id,snippet&channelId='+channel+'&maxResults='+maxRes+'&key='+key

    this.api.Get(url, true).then((res: any) => {

      let data = res.items
      let output = []
      data.forEach(item => {
        if(item.id.videoId) {
          let videoLink = 'https://www.youtube.com/embed/'+item.id.videoId
          output.push({
            url: this.domSanitizer.bypassSecurityTrustResourceUrl(videoLink),
            snippet: item.snippet
          })
        }
      });
      this.tutorialVideo = output;
      console.log(this.tutorialVideo);
    })
    // this.tutorialVideo.push(data)
    // i.listLinkYT = this.domSanitizer.bypassSecurityTrustResourceUrl(i.id.videoId);

    // this.tutorialVideo = output

  }

}
