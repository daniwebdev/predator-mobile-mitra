import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ApiServiceProvider } from "../../providers/api-service/api-service";

import { KonsumenPage } from "../@konsumen/konsumen/konsumen";
/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  public result = [];
  total_dp = '';
  total_kredit_bulan = ''
  total_cicilan = ''

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public api: ApiServiceProvider,
    public loading: LoadingController
  ) {
  }

  ionViewWillEnter() {
    this.getList()
  }


  getList() {
    let loading = this.loading.create({
      content: 'Mohon Tunggu...'
    });
    this.api.Get('pemesanan/cart/get').then((res: any) => {

      loading.dismiss();
      this.result             = res.data.result
      this.total_kredit_bulan = res.data.total_kredit_bulan
      this.total_dp           = res.data.total_dp 
      this.total_cicilan      = res.data.total_cicilan

    })
  }

  goProses() {
    if(this.result.length > 0){
      this.navCtrl.push(KonsumenPage)
    } else {
      alert('Tidak ada barang di keranjang.')
    }
  }

  hapusCart(idCart) {
    let loading = this.loading.create({
      content: 'Mohon Tunggu...'
    });
    loading.present();
    this.api.Get('pemesanan/cart/delete?cart_id='+idCart).then((res: any) => {
      loading.dismiss();
      if(res.status) {
        this.getList()
      }
    }).catch((error) => {
      loading.dismiss();
    })
  }
}
