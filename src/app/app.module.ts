import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, Platform } from 'ionic-angular';
import { MyApp } from './app.component';

import { AccountPage } from '../pages/account/account';
import { HomePage, MenuPage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPageModule } from "../pages/login/login.module";
import { ListProdukPageModule } from "../pages/@persediaan/list-produk/list-produk.module";
import { DetailProdukPageModule } from "../pages/@persediaan/detail-produk/detail-produk.module";
import { CartPageModule } from "../pages/cart/cart.module";
import { KonsumenPageModule } from "../pages/@konsumen/konsumen/konsumen.module";
import { KonsumenDetailPageModule } from "../pages/@konsumen/konsumen-detail/konsumen-detail.module";
import { KonsumenAddPageModule } from "../pages/@konsumen/konsumen-add/konsumen-add.module";
import { DetailOrderPageModule } from "../pages/@pemesanan/detail-order/detail-order.module";
import { BantuanPageModule } from "../pages/bantuan/bantuan.module";
import { BeritaPageModule } from "../pages/berita/berita.module";

import { TasModule } from "../pages/@tas/tas.module";

import { HttpClientModule } from "@angular/common/http";
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { IonicStorageModule } from "@ionic/storage";
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { IntroPageModule } from '../pages/intro/intro.module';
import { IonicImageLoader, ImgLoaderComponent } from "ionic-image-loader";

@NgModule({
  declarations: [
    MyApp,
    AccountPage,
    HomePage,
    TabsPage,
    MenuPage
  ],
  imports: [
    BrowserModule,
    LoginPageModule,
    ListProdukPageModule,
    DetailProdukPageModule,
    CartPageModule,
    KonsumenAddPageModule,
    KonsumenPageModule,
    KonsumenDetailPageModule,
    BantuanPageModule,
    BeritaPageModule,
    IntroPageModule,
    DetailOrderPageModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    TasModule,
    IonicModule.forRoot(MyApp, {
        tabsHideOnSubPages: true,
    }),
    IonicImageLoader.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AccountPage,
    HomePage,
    TabsPage,
    MenuPage,
    ImgLoaderComponent
  ],
  providers: [
    StatusBar,
    FileTransfer,
    File,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiServiceProvider
  ]
})
export class AppModule {}
